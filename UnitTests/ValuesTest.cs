﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using MyCholesterol.Core;
using Should;

namespace MyCholesterol.Core.UnitTests
{
    [TestFixture]
    public class ValuesTest
    {
        [Test]
        public void Constructor_when_OnlyTotalIsGiven()
        {
            ushort total = 100;

            Values values = new Values(total);

            Assert.IsNotNull(values);

            values.Total.ShouldEqual(total);
            values.LDL.ShouldEqual((ushort)0);
            values.HDL.ShouldEqual((ushort)0);
            values.Triglycerides.ShouldEqual((ushort)0);
        }

        [Test]
        public void Constructor_when_VAluesAreGiven()
        {
            ushort total = 100;
            ushort ldl = 101;
            ushort hdl = 102;
            ushort triglycerides = 103;

            Values values = new Values(total, ldl, hdl, triglycerides);

            Assert.IsNotNull(values);

            values.Total.ShouldEqual(total);
            values.LDL.ShouldEqual(ldl);
            values.HDL.ShouldEqual(hdl);
            values.Triglycerides.ShouldEqual(triglycerides);
        }

    }
}
