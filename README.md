# MyCholesterol Core #

This is library containing the basic classes of MyCholesterol project.

## Content ##

* Values class that contains cholesterol values like "Total", "LDL", "HDL" and "Tryglicerides".
* LdlCalculator class that can calculate LDL value by other values using Friedwald formula.

### Values class ###
This class must contains total cholesterol value at minimum, other values are optional.
Unit measure for values is intended to be mg/dl.

## Developer notes ##
The Visual Studio solution is ready to create a NuGet package using nuget.exe by a batch file.