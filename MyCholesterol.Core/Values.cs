﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyCholesterol.Core
{
    [Serializable]
    public class Values
    {
        public ushort Total { get; set; }
        public ushort LDL { get; set; } = 0;

        public ushort HDL { get; set; } = 0;
        public ushort Triglycerides { get; set; } = 0;

        public Values(ushort total, ushort ldl = 0, ushort hdl = 0, ushort triglycerides = 0)
        {
            Total = total;
            LDL = ldl;
            HDL = hdl;
            Triglycerides = triglycerides;
        }
    }
}
